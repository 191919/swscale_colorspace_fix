#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

//#define COLORSPACE_SWSCALE_DEFAULT
#define COLORSPACE_FLASH

#ifdef COLORSPACE_SWSCALE_DEFAULT
#define EyR 0.299
#define EyG 0.587
#define EyB 0.114

#define EuR 0.500
#define EuG -0.419
#define EuB -0.081

#define EvR -0.169
#define EvG -0.331
#define EvB 0.500
#endif

#ifdef COLORSPACE_FLASH
#define EyR 0.257
#define EyG 0.504
#define EyB 0.098

#define EuR -0.148
#define EuG -0.291
#define EuB 0.439

#define EvR 0.439
#define EvG -0.368
#define EvB -0.071
#endif

#undef RGB2YUV_SHIFT
#define RGB2YUV_SHIFT 7
const uint16_t co7[] = {
    ((int)(EyB*219/255*(1<<RGB2YUV_SHIFT)+0.5)),
    ((int)(EuB*224/255*(1<<RGB2YUV_SHIFT)+0.5)),
    ((int)(EvB*224/255*(1<<RGB2YUV_SHIFT)+0.5)),

    ((int)(EyG*219/255*(1<<RGB2YUV_SHIFT)+0.5)),
    ((int)(EuG*224/255*(1<<RGB2YUV_SHIFT)+0.5)),
    ((int)(EvG*224/255*(1<<RGB2YUV_SHIFT)+0.5)),

    ((int)(EyR*219/255*(1<<RGB2YUV_SHIFT)+0.5)),
    ((int)(EuR*224/255*(1<<RGB2YUV_SHIFT)+0.5)),
    ((int)(EvR*224/255*(1<<RGB2YUV_SHIFT)+0.5)) };

#undef RGB2YUV_SHIFT
#define RGB2YUV_SHIFT 15
const uint16_t co15[] = {
    ((int)(EyB*219/255*(1<<RGB2YUV_SHIFT)+0.5)),
    ((int)(EuB*224/255*(1<<RGB2YUV_SHIFT)+0.5)),
    ((int)(EvB*224/255*(1<<RGB2YUV_SHIFT)+0.5)),

    ((int)(EyG*219/255*(1<<RGB2YUV_SHIFT)+0.5)),
    ((int)(EuG*224/255*(1<<RGB2YUV_SHIFT)+0.5)),
    ((int)(EvG*224/255*(1<<RGB2YUV_SHIFT)+0.5)),

    ((int)(EyR*219/255*(1<<RGB2YUV_SHIFT)+0.5)),
    ((int)(EuR*224/255*(1<<RGB2YUV_SHIFT)+0.5)),
    ((int)(EvR*224/255*(1<<RGB2YUV_SHIFT)+0.5)) };

int main()
{
    printf(
"#define RGB2YUV_SHIFT 15\n"
"#define BY ( (int)(%.3f*219/255*(1<<RGB2YUV_SHIFT)+0.5))\n"
"#define BV ( (int)(%.3f*224/255*(1<<RGB2YUV_SHIFT)+0.5))\n"
"#define BU ( (int)(%.3f*224/255*(1<<RGB2YUV_SHIFT)+0.5))\n"
"#define GY ( (int)(%.3f*219/255*(1<<RGB2YUV_SHIFT)+0.5))\n"
"#define GV ( (int)(%.3f*224/255*(1<<RGB2YUV_SHIFT)+0.5))\n"
"#define GU ( (int)(%.3f*224/255*(1<<RGB2YUV_SHIFT)+0.5))\n"
"#define RY ( (int)(%.3f*219/255*(1<<RGB2YUV_SHIFT)+0.5))\n"
"#define RV ( (int)(%.3f*224/255*(1<<RGB2YUV_SHIFT)+0.5))\n"
"#define RU ( (int)(%.3f*224/255*(1<<RGB2YUV_SHIFT)+0.5))\n\n",
    EyB,EuB,EvB,
    EyG,EuG,EvG,
    EyR,EuR,EvR);

    printf("#ifdef FAST_BGR2YV12\n");

    printf("DECLARE_ALIGNED(8, const uint64_t, ff_bgr2YCoeff)   = 0x0000%04X%04X%04XULL;\n", co7[6], co7[3], co7[0]);
    printf("DECLARE_ALIGNED(8, const uint64_t, ff_bgr2UCoeff)   = 0x0000%04X%04X%04XULL;\n", co7[8], co7[5], co7[2]);
    printf("DECLARE_ALIGNED(8, const uint64_t, ff_bgr2VCoeff)   = 0x0000%04X%04X%04XULL;\n", co7[7], co7[4], co7[1]);

    printf("#else\n");

    printf("DECLARE_ALIGNED(8, const uint64_t, ff_bgr2YCoeff)   = 0x0000%04X%04X%04XULL;\n", co15[6], co15[3], co15[0]);
    printf("DECLARE_ALIGNED(8, const uint64_t, ff_bgr2UCoeff)   = 0x0000%04X%04X%04XULL;\n", co15[8], co15[5], co15[2]);
    printf("DECLARE_ALIGNED(8, const uint64_t, ff_bgr2VCoeff)   = 0x0000%04X%04X%04XULL;\n", co15[7], co15[4], co15[1]);

    printf("#endif\n");
    printf(
"\n"
"DECLARE_ALIGNED(8, const uint64_t, ff_bgr2YOffset)  = 0x1010101010101010ULL;\n"
"DECLARE_ALIGNED(8, const uint64_t, ff_bgr2UVOffset) = 0x8080808080808080ULL;\n"
"DECLARE_ALIGNED(8, const uint64_t, ff_w1111)        = 0x0001000100010001ULL;\n"
"\n"
    );
    printf("DECLARE_ASM_CONST(8, uint64_t, ff_bgr24toY1Coeff) = %04X0000%04X%04XULL;\n", co15[0], co15[3], co15[0]);
    printf("DECLARE_ASM_CONST(8, uint64_t, ff_bgr24toY2Coeff) = %04X%04X0000%04XULL;\n", co15[6], co15[3], co15[6]);
    printf("DECLARE_ASM_CONST(8, uint64_t, ff_rgb24toY1Coeff) = %04X%04X0000%04XULL;\n", co15[6], co15[3], co15[6]);
    printf("DECLARE_ASM_CONST(8, uint64_t, ff_rgb24toY2Coeff) = %04X0000%04X%04XULL;\n", co15[0], co15[3], co15[0]);

    printf(
"\nDECLARE_ASM_CONST(8, uint64_t, ff_bgr24toYOffset) = 0x0008400000084000ULL;\n"
"\n"
"DECLARE_ASM_CONST(8, uint64_t, ff_bgr24toUV)[2][4] = {\n"
"    {");

    printf("%04X0000%04X%04XULL, ", co15[2], co15[5], co15[2]);
    printf("%04X%04X0000%04XULL, ", co15[8], co15[5], co15[8]);
    printf("%04X0000%04X%04XULL, ", co15[1], co15[4], co15[1]);
    printf("%04X%04X0000%04XULL},", co15[7], co15[4], co15[7]);

    printf("\n    {");

    printf("%04X0000%04X%04XULL, ", co15[8], co15[5], co15[8]);
    printf("%04X%04X0000%04XULL, ", co15[2], co15[5], co15[2]);
    printf("%04X0000%04X%04XULL, ", co15[2], co15[4], co15[2]);
    printf("%04X%04X0000%04XULL},", co15[7], co15[4], co15[7]);

    printf("\n};\n");



//    {int i;for(i=0;i<9;i++)printf("%d %04X\n",i,co15[i]);printf("\n");}
}